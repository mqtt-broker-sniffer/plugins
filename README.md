# Plugins

Those plugins are make for the <a href="https://gitlab.com/mqtt-broker-sniffer/client">MQTT Viewer</a> project.

The plugins allow to add additional features to the MQTT viewer through several mount points.

## Usage

The list of plugins that provides a feature named `<feature_name>`.

```Javascript
import fetchPlugins from 'plugins/loader.js';
const plugins = await Promise.all(await fetchPlugins("<feature_name>"));
```

## package.json

The `package.json` file of the plugin has to be present at the root of the plugin package and have the following scheme.

```JSON
{
    "name": "<plugin_name>",
    "features": {
        "<feature_name>": "<feature_entry_point>"
    },
    "dependencies": {
        "<dependency_name>": "<dependency_version>"
    }
}
```

The feature name has to be either `messageContentViewer`, `messageContentVerifier`, `messageFilters`, and/or `additionalFeature`.

The returned feature has to be exported as default in the entry point file.

The dependencies are the `npm` dependencies that this plugin needs to run.

## fetchPlugins(feature)

Fetch and import all plugins with the entered feature

### Arguments

| Field   | Type     | Default | Description                                 |
| ------- | -------- | ------- | ------------------------------------------- |
| feature | `string` |         | the feature that the plugins should support |

### Returns

`Promise<[Promise<{name: string, import: *}>]>`: the imported features with the name of their package

# Features

The list of possible plugin features and their arguments and return type.

## messageContentViewer

The component that will be displayed in the content of message. Has to implement `messageContentVerifier` to select the messages which will use this componenent.

### Properties

| Property | PropType | Description                     |
| -------- | -------- | ------------------------------- |
| content  | `Buffer` | The content of the MQTT message |

### Returns

`JSX.Element`: The element

## messageFilters

The component that will be displayed in the filter panel. Has to implement `messageContentVerifier` to select the messages that will be affected by the filter. The filter should accept every messages at the first load of the componenent.

### Properties

| Property | PropType                                              | Description                               |
| -------- | ----------------------------------------------------- | ----------------------------------------- |
| onChange | `(`<a href="../model/Filter.md">Filter</a>`) => void` | The callback when a new filter is created |

### Returns

`JSX.Element`: The element

## messageContentVerifier

Check if a message content is manage by this plugin or not

### Arguments

| Field   | Type     | Description                     |
| ------- | -------- | ------------------------------- |
| content | `Buffer` | the content of the MQTT message |

### Returns

`boolean`: true if the message content is managed by this plugin

## additionalFeature

The component will be added in an additional pannel of the published messages.

### Properties

| Property | PropType                                          | Description      |
| -------- | ------------------------------------------------- | ---------------- |
| broker   | <a href="../model/BrokerState.md">BrokerState</a> | The broker state |

### Returns

`JSX.Element`: The element
